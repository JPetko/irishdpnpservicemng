Project Name:
IrisHD_PnpServiceMng.exe

Language:
C#

Description:
Application for installation, starting, stopping, and removal of the Iris HD Pnp Service. The Service will manage
video filter registration of the Iris HD camera upon connection and disconnection.


How to Use: Click install, this will copy the pnpservice.exe to the Iris HD install directory. register the service
using sc.exe, then start the service automatically.

Author: 
James Petko (petko.james@gmail.com)

Initial Build Date:
08/31/2018