﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Threading;
using System.IO;

namespace IrisHD_PnpServiceMng
{
    public partial class Form1 : Form
    {
        private ServiceController sc = new ServiceController("Iris HD Pnp Service");
        public Form1()
        {
            InitializeComponent();
            
            string servicestatus=string.Empty;
            lab_StatusReadout.Text = ServiceCheck();
        }

        private void pb_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pb_Install_Click(object sender, EventArgs e)
        {
            ButtonsActive(false);
            lab_StatusReadout.Text = "Installing...";
            this.Update();
            try
            {
                File.WriteAllBytes(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Iris HD\\pnpService.exe"
                    , IrisHD_PnpServiceMng.Properties.Resources.pnpService);
                Thread.Sleep(500);
            }
            catch { }
            (new Thread(() =>
            {
                
                try
                {                                       
                    Process p = new Process();
                    p.StartInfo.FileName = "sc.exe";
                    p.StartInfo.Arguments = "create \"Iris HD Pnp Service\" binPath= \"" + Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) +
                        "\\Iris HD\\pnpService.exe\" start= auto";
                    p.StartInfo.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.SystemX86);
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.Start();                    
                    
                }
            catch { }
            })).Start();
            Thread.Sleep(3000);
            lab_StatusReadout.Text = "Installed, Attempting to start now";
            this.pb_Start_Click(sender, e);
            ButtonsActive(true);
        }

        private void lab_Install_Click(object sender, EventArgs e)
        {
            this.pb_Install_Click(sender, e);
        }

        private void pb_Start_Click(object sender, EventArgs e)
        {
            ButtonsActive(false);
            lab_StatusReadout.Text = "Starting";
            (new Thread(() =>
            {
                try
                {
                    sc.Start();                    
                }
                catch { } 
            })).Start();
            sc.WaitForStatus(ServiceControllerStatus.Running);
            lab_StatusReadout.Text = "Running";
            ButtonsActive(true);
        }

        private void lab_Start_Click(object sender, EventArgs e)
        {
            this.pb_Start_Click(sender, e);
        }

        private void pb_Delete_Click(object sender, EventArgs e)
        {
            ButtonsActive(false);
            try
            {
                int counter = 0;
                if (sc.Status != ServiceControllerStatus.Stopped)
                {
                    lab_StatusReadout.Text = "Please Stop the Service";
                    return;
                }
                else
                {
                    lab_StatusReadout.Text = "Removing, this may take a moment.";
                }
                this.Update();
            (new Thread(() =>
            {

                try
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "sc.exe";
                    p.StartInfo.Arguments = "delete \"Iris HD Pnp Service\"";
                    p.StartInfo.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.SystemX86);
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.Start();

                }
                catch { }
            })).Start();
                while (ServiceCheck() != "Not installed, please click Install." && counter != 14)
                {
                    Thread.Sleep(1000);
                    counter++;
                }
                if (counter == 30)
                    PromptForRestart();
                else
                    lab_StatusReadout.Text = "Removed, You may now reinstall";
            }
            catch { }
            ButtonsActive(true);
        }

        private void lab_Delete_Click(object sender, EventArgs e)
        {
            this.pb_Delete_Click(sender, e);
        }

        private void pb_Stop_Click(object sender, EventArgs e)
        {
            ButtonsActive(false);
            lab_StatusReadout.Text = "Stopping, this may take a moment";
            this.Update();
            (new Thread(() =>
            {
                try
                {
                    sc.Stop();
                }
                catch { }
                

            })).Start();
            
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
            lab_StatusReadout.Text = "Stopped";
            ButtonsActive(true);
        }

        private void lab_Stop_Click(object sender, EventArgs e)
        {
            this.pb_Stop_Click(sender, e);
        }

        private string ServiceCheck()
        {            
            try
            {
                if (sc.Status == ServiceControllerStatus.Running)
                {
                   
                    return "Running";
                }

                if (sc.Status == ServiceControllerStatus.Stopped)
                {
                   
                    return "Stopped"; 
                }

                if (sc.Status == ServiceControllerStatus.Paused)
                {
                    return "Paused";
                }

                if (sc.Status == ServiceControllerStatus.StopPending)
                {
                    return "Stopping, Please Wait";                    
                }

                if (sc.Status == ServiceControllerStatus.StartPending)
                {
                    return "Starting";                    
                }
            }
            catch
            {
                
                return "Not installed, please click Install.";

            }
            return "Not installed, please click Install.";
        }
        private void PromptForRestart()
        {
            if (MessageBox.Show(this, "The Service is Marked for Deletion\r\nPlease Restart the Computer",string.Empty, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Process p = new Process();
                p.StartInfo.FileName = "shutdown.exe";
                p.StartInfo.Arguments = "/r /f /t 3";
                p.Start();
            }
            else
                Application.Exit();
        }
        private void ButtonsActive(bool active)
        {
            pb_Delete.Enabled = active;
            lab_Delete.Enabled = active;

            pb_Exit.Enabled = active;

            pb_Install.Enabled = active;
            lab_Install.Enabled = active;

            pb_Start.Enabled = active;
            lab_Start.Enabled = active;

            pb_Stop.Enabled = active;
            lab_Stop.Enabled = active;

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Process.Start("https://digi-doc.com/contact-us");
            Process.Start("https://www.gotoassist.com/ph/digidoc");
        }
    }   
}
