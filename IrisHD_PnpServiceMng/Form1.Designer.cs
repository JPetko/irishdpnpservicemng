﻿namespace IrisHD_PnpServiceMng
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lab_Status = new System.Windows.Forms.Label();
            this.lab_Install = new System.Windows.Forms.Label();
            this.lab_Start = new System.Windows.Forms.Label();
            this.lab_Delete = new System.Windows.Forms.Label();
            this.lab_Stop = new System.Windows.Forms.Label();
            this.lab_StatusReadout = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pb_Install = new System.Windows.Forms.PictureBox();
            this.pb_Delete = new System.Windows.Forms.PictureBox();
            this.pb_Stop = new System.Windows.Forms.PictureBox();
            this.pb_Start = new System.Windows.Forms.PictureBox();
            this.pb_Exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Install)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Exit)).BeginInit();
            this.SuspendLayout();
            // 
            // lab_Status
            // 
            this.lab_Status.AutoSize = true;
            this.lab_Status.Location = new System.Drawing.Point(9, 191);
            this.lab_Status.Name = "lab_Status";
            this.lab_Status.Size = new System.Drawing.Size(114, 13);
            this.lab_Status.TabIndex = 4;
            this.lab_Status.Text = "IrisHD PnP Service is: ";
            // 
            // lab_Install
            // 
            this.lab_Install.AutoSize = true;
            this.lab_Install.Location = new System.Drawing.Point(48, 100);
            this.lab_Install.Name = "lab_Install";
            this.lab_Install.Size = new System.Drawing.Size(73, 13);
            this.lab_Install.TabIndex = 12;
            this.lab_Install.Text = "Install Service";
            this.lab_Install.Click += new System.EventHandler(this.lab_Install_Click);
            // 
            // lab_Start
            // 
            this.lab_Start.AutoSize = true;
            this.lab_Start.Location = new System.Drawing.Point(50, 146);
            this.lab_Start.Name = "lab_Start";
            this.lab_Start.Size = new System.Drawing.Size(68, 13);
            this.lab_Start.TabIndex = 13;
            this.lab_Start.Text = "Start Service";
            this.lab_Start.Click += new System.EventHandler(this.lab_Start_Click);
            // 
            // lab_Delete
            // 
            this.lab_Delete.AutoSize = true;
            this.lab_Delete.Location = new System.Drawing.Point(191, 100);
            this.lab_Delete.Name = "lab_Delete";
            this.lab_Delete.Size = new System.Drawing.Size(86, 13);
            this.lab_Delete.TabIndex = 14;
            this.lab_Delete.Text = "Remove Service";
            this.lab_Delete.Click += new System.EventHandler(this.lab_Delete_Click);
            // 
            // lab_Stop
            // 
            this.lab_Stop.AutoSize = true;
            this.lab_Stop.Location = new System.Drawing.Point(191, 146);
            this.lab_Stop.Name = "lab_Stop";
            this.lab_Stop.Size = new System.Drawing.Size(68, 13);
            this.lab_Stop.TabIndex = 15;
            this.lab_Stop.Text = "Stop Service";
            this.lab_Stop.Click += new System.EventHandler(this.lab_Stop_Click);
            // 
            // lab_StatusReadout
            // 
            this.lab_StatusReadout.AutoSize = true;
            this.lab_StatusReadout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_StatusReadout.Location = new System.Drawing.Point(146, 191);
            this.lab_StatusReadout.Name = "lab_StatusReadout";
            this.lab_StatusReadout.Size = new System.Drawing.Size(0, 13);
            this.lab_StatusReadout.TabIndex = 16;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::IrisHD_PnpServiceMng.Properties.Resources.LargeTestBMP3;
            this.pictureBox2.Location = new System.Drawing.Point(12, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(192, 86);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb_Install
            // 
            this.pb_Install.Image = global::IrisHD_PnpServiceMng.Properties.Resources.installIconBLUE;
            this.pb_Install.Location = new System.Drawing.Point(12, 91);
            this.pb_Install.Name = "pb_Install";
            this.pb_Install.Size = new System.Drawing.Size(30, 30);
            this.pb_Install.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Install.TabIndex = 9;
            this.pb_Install.TabStop = false;
            this.pb_Install.Click += new System.EventHandler(this.pb_Install_Click);
            // 
            // pb_Delete
            // 
            this.pb_Delete.Image = global::IrisHD_PnpServiceMng.Properties.Resources.NukeIconORANGE;
            this.pb_Delete.Location = new System.Drawing.Point(155, 91);
            this.pb_Delete.Name = "pb_Delete";
            this.pb_Delete.Size = new System.Drawing.Size(30, 30);
            this.pb_Delete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Delete.TabIndex = 8;
            this.pb_Delete.TabStop = false;
            this.pb_Delete.Click += new System.EventHandler(this.pb_Delete_Click);
            // 
            // pb_Stop
            // 
            this.pb_Stop.Image = global::IrisHD_PnpServiceMng.Properties.Resources.StopIconRED;
            this.pb_Stop.Location = new System.Drawing.Point(155, 140);
            this.pb_Stop.Name = "pb_Stop";
            this.pb_Stop.Size = new System.Drawing.Size(30, 30);
            this.pb_Stop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Stop.TabIndex = 7;
            this.pb_Stop.TabStop = false;
            this.pb_Stop.Click += new System.EventHandler(this.pb_Stop_Click);
            // 
            // pb_Start
            // 
            this.pb_Start.Image = global::IrisHD_PnpServiceMng.Properties.Resources.startIconGreen;
            this.pb_Start.Location = new System.Drawing.Point(12, 140);
            this.pb_Start.Name = "pb_Start";
            this.pb_Start.Size = new System.Drawing.Size(30, 30);
            this.pb_Start.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Start.TabIndex = 6;
            this.pb_Start.TabStop = false;
            this.pb_Start.Click += new System.EventHandler(this.pb_Start_Click);
            // 
            // pb_Exit
            // 
            this.pb_Exit.Image = global::IrisHD_PnpServiceMng.Properties.Resources.CloseIconRED;
            this.pb_Exit.Location = new System.Drawing.Point(321, 12);
            this.pb_Exit.Name = "pb_Exit";
            this.pb_Exit.Size = new System.Drawing.Size(30, 30);
            this.pb_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Exit.TabIndex = 5;
            this.pb_Exit.TabStop = false;
            this.pb_Exit.Click += new System.EventHandler(this.pb_Exit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(363, 225);
            this.Controls.Add(this.lab_StatusReadout);
            this.Controls.Add(this.lab_Stop);
            this.Controls.Add(this.lab_Delete);
            this.Controls.Add(this.lab_Start);
            this.Controls.Add(this.lab_Install);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pb_Install);
            this.Controls.Add(this.pb_Delete);
            this.Controls.Add(this.pb_Stop);
            this.Controls.Add(this.pb_Start);
            this.Controls.Add(this.pb_Exit);
            this.Controls.Add(this.lab_Status);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Iris HD Pnp Service Manager";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Install)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Exit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lab_Status;
        private System.Windows.Forms.PictureBox pb_Exit;
        private System.Windows.Forms.PictureBox pb_Start;
        private System.Windows.Forms.PictureBox pb_Stop;
        private System.Windows.Forms.PictureBox pb_Delete;
        private System.Windows.Forms.PictureBox pb_Install;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lab_Install;
        private System.Windows.Forms.Label lab_Start;
        private System.Windows.Forms.Label lab_Delete;
        private System.Windows.Forms.Label lab_Stop;
        private System.Windows.Forms.Label lab_StatusReadout;
    }
}

